package au.com.parki.admin.dashboard.funqy.dto;

public class DashboardRequest {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
