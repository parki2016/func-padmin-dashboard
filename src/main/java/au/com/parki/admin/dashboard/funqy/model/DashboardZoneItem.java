package au.com.parki.admin.dashboard.funqy.model;

import java.math.BigDecimal;

public class DashboardZoneItem {

    private Long id;
    private String zone;
    private Long carParkType;
    private Long countTickets;
    private BigDecimal sumRevenue;
    private BigDecimal sumSurcharge;

    public static DashboardZoneItem getInstance(Object[] row) {
        DashboardZoneItem target = new DashboardZoneItem();
        target.setId(Long.valueOf(row[0].toString()));
        target.setZone(row[1].toString());
        target.setCarParkType(Long.valueOf(row[2].toString()));
        target.setCountTickets(Long.valueOf(row[3].toString()));
        target.setSumRevenue(row[4] == null ? BigDecimal.ZERO : new BigDecimal(row[4].toString()));
        target.setSumSurcharge(row[5] == null ? BigDecimal.ZERO : new BigDecimal(row[5].toString()));
        return target;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public Long getCarParkType() {
        return carParkType;
    }

    public void setCarParkType(Long carParkType) {
        this.carParkType = carParkType;
    }

    public Long getCountTickets() {
        return countTickets;
    }

    public void setCountTickets(Long countTickets) {
        this.countTickets = countTickets;
    }

    public BigDecimal getSumRevenue() {
        return sumRevenue;
    }

    public void setSumRevenue(BigDecimal sumRevenue) {
        this.sumRevenue = sumRevenue;
    }

    public BigDecimal getSumSurcharge() {
        return sumSurcharge;
    }

    public void setSumSurcharge(BigDecimal sumSurcharge) {
        this.sumSurcharge = sumSurcharge;
    }
}
