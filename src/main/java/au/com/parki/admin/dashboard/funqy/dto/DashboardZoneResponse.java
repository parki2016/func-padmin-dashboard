package au.com.parki.admin.dashboard.funqy.dto;

import au.com.parki.admin.dashboard.funqy.model.DashboardZone;

import java.math.BigDecimal;

public class DashboardZoneResponse {

    private final Long id;
    private final String zoneName;
    private final String zoneType;
    private Long count;
    private BigDecimal revenue;

    private DashboardZoneResponse(Long id, String zoneName, String zoneType, Long count, BigDecimal revenue) {
        this.id = id;
        this.zoneName = zoneName;
        this.zoneType = zoneType;
        this.count = count;
        this.revenue = revenue;
    }

    public static DashboardZoneResponse getInstance(DashboardZone dashboardZone) {
        return new DashboardZoneResponse(dashboardZone.getId(), dashboardZone.getZoneName(), dashboardZone.getCarParkTypeFk().equals(2L) ? "IPark" : "Parki",
                BigDecimal.ZERO.longValue(), BigDecimal.ZERO);
    }

    public Long getId() {
        return id;
    }

    public String getZoneName() {
        return zoneName;
    }

    public String getZoneType() {
        return zoneType;
    }

    public Long getCount() {
        return count;
    }

    public BigDecimal getRevenue() {
        return revenue;
    }

    public void updateCount(Long count) {
        if(count != null) this.count += count;
    }

    public void updateRevenue(BigDecimal revenue) {
        if(revenue != null) this.revenue = this.revenue.add(revenue);
    }
}
