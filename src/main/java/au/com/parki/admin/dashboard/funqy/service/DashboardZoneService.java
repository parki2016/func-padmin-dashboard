package au.com.parki.admin.dashboard.funqy.service;

import au.com.parki.admin.dashboard.funqy.model.DashboardZone;
import au.com.parki.admin.dashboard.funqy.model.DashboardZoneItem;
import io.quarkus.runtime.Startup;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Startup
@Singleton
public class DashboardZoneService {

    @Inject
    EntityManager em;

    private static final String GET_PARKI_TICKETS = "select z.id 'id', z.zone_name 'zone', z.car_park_type_fk 'carParkType', count(*) 'countTickets',  sum(t.ticket_fee) 'sumRevenue', sum(t.surcharge_fee) 'sumSurcharge' " +
            "from ticket t inner join organisation org on t.organisation_fk = org.id inner join zone z on t.zone_fk = z.id where org.id = %d " +
            "and z.archived <> 1 and car_park_type_fk = 1 and parki_app = 1 and t.created_date <> t.expiry_date " +
            "and date(CONVERT_TZ(t.created_date, 'UTC', org.timezone)) = date(CONVERT_TZ(curdate(), 'UTC', org.timezone)) group by z.id";
    public List<DashboardZoneItem> getParkiTickets(Long orgId) {
        return getDashboardItem(GET_PARKI_TICKETS, orgId);
    }

    private static final String GET_BEACON_TICKETS = "select z.id 'id', z.zone_name 'zone', z.car_park_type_fk 'carParkType', count(*) 'countTickets',  sum(t.amount) 'sumRevenue', sum(t.surcharge) 'sumSurcharge' " +
            "from beacon_ticket t inner join zone z on t.zone_fk = z.id inner join organisation org on z.organisation_fk = org.id where org.id = %d " +
            "and z.archived <> 1 and date(CONVERT_TZ(t.start_date, 'UTC', org.timezone)) = date(CONVERT_TZ(curdate(), 'UTC', org.timezone)) group by z.id";
    public List<DashboardZoneItem> getBeaconTickets(Long orgId) {
        return getDashboardItem(GET_BEACON_TICKETS, orgId);
    }

    private static final String GET_IPARK_TICKETS = "select z.id 'id', z.zone_name 'zone', z.car_park_type_fk 'carParkType', count(*) 'countTickets',  sum(t.amount) 'sumRevenue', sum(t.surcharge) 'sumSurcharge' " +
            "from ipark_ticket t inner join ipark_movement_event ent on t.entry_fk = ent.id inner join zone z on ent.zone_fk = z.id inner join organisation org on z.organisation_fk = org.id " +
            "where org.id = %d and z.archived <> 1 and t.reservation_fk is null and t.payment_auth_fk is not null " +
            "and date(CONVERT_TZ(t.exitBeforeDateTime, 'UTC', org.timezone)) = date(CONVERT_TZ(curdate(), 'UTC', org.timezone)) group by z.id";
    public List<DashboardZoneItem> getIParkTickets(Long orgId) {
        return getDashboardItem(GET_IPARK_TICKETS, orgId);
    }

    private List<DashboardZoneItem> getDashboardItem(String sqlTemplate, Long orgId) {
        List<Object[]> list = em.createNativeQuery(String.format(sqlTemplate, orgId)).getResultList();
        if(list != null) return list.stream().map(DashboardZoneItem::getInstance).collect(Collectors.toList());
        return Collections.EMPTY_LIST;
    }

    private static final String ACTIVE_ZONE = "Select z from DashboardZone z where z.organisationFk = :orgId and z.archived = false";
    private static final String ORG_ID = "orgId";
    public List<DashboardZone> findByOrganisationFkAndArchivedFalse(Long organisationFk) {
        return em.createQuery(ACTIVE_ZONE, DashboardZone.class).setParameter(ORG_ID, organisationFk).getResultList();
    }
}
