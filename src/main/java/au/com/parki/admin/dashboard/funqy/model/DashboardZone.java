package au.com.parki.admin.dashboard.funqy.model;

import javax.persistence.*;

@Entity
@Table(name = "zone")
public class DashboardZone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "zone_name")
    private String zoneName;

    @Column
    private Boolean archived;

    @Column(name = "parki_app")
    private Boolean allowParkiApp;

    @Column(name = "organisation_fk", updatable = false)
    private Long organisationFk;

    @Column(name="car_park_type_fk")
    private Long carParkTypeFk;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public Boolean getArchived() {
        return archived;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    public Boolean getAllowParkiApp() {
        return allowParkiApp;
    }

    public void setAllowParkiApp(Boolean allowParkiApp) {
        this.allowParkiApp = allowParkiApp;
    }

    public Long getOrganisationFk() {
        return organisationFk;
    }

    public void setOrganisationFk(Long organisationFk) {
        this.organisationFk = organisationFk;
    }

    public Long getCarParkTypeFk() {
        return carParkTypeFk;
    }

    public void setCarParkTypeFk(Long carParkTypeFk) {
        this.carParkTypeFk = carParkTypeFk;
    }
}
