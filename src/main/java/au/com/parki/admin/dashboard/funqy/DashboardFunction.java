package au.com.parki.admin.dashboard.funqy;

import au.com.parki.ParkiTokenUtil;
import au.com.parki.admin.dashboard.funqy.dto.DashboardRequest;
import au.com.parki.admin.dashboard.funqy.dto.DashboardZoneResponse;
import au.com.parki.admin.dashboard.funqy.model.DashboardZone;
import au.com.parki.admin.dashboard.funqy.model.DashboardZoneItem;
import au.com.parki.admin.dashboard.funqy.service.DashboardZoneService;
import io.quarkus.funqy.Funq;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DashboardFunction {

    @Inject
    ParkiTokenUtil tokenUtil;

    @Inject
    DashboardZoneService dashboardZoneService;

    @Transactional
    @Funq
    public List<DashboardZoneResponse> getDashboardZones(DashboardRequest request) {
/*
        System.out.println("headers : "+dashboardRequest.getHeaders());
        System.out.println("My-Auth : "+dashboardRequest.getHeaders().get(AUTH));
        DashboardRequest request = ((DashboardRequest) dashboardRequest.getPayload());
*/
        String tokenStr = request.getToken();
        System.out.println("GOT Token : " + tokenStr);
        Long org = tokenUtil.getParkiUserToken(tokenStr).getOrg();
        Map<Long, DashboardZoneResponse> map = dashboardZoneService.findByOrganisationFkAndArchivedFalse(org).stream()
                .collect(Collectors.toMap(DashboardZone::getId, DashboardZoneResponse::getInstance));
        updateResponse(map, dashboardZoneService.getParkiTickets(org));
        updateResponse(map, dashboardZoneService.getBeaconTickets(org));
        updateResponse(map, dashboardZoneService.getIParkTickets(org));
        return new ArrayList<>(map.values()).stream().sorted(Comparator.comparing(DashboardZoneResponse::getZoneName)).collect(Collectors.toList());

    }

    private static void updateResponse(Map<Long, DashboardZoneResponse> map, List<DashboardZoneItem> items){
        if(items == null || items.isEmpty()) return;
        items.forEach(dashboardZoneItem -> {
            DashboardZoneResponse response = map.get(dashboardZoneItem.getId());
            if(response != null) {
                response.updateCount(dashboardZoneItem.getCountTickets());
                response.updateRevenue(dashboardZoneItem.getSumRevenue().add(dashboardZoneItem.getSumSurcharge()));
            }
        });
    }

}
