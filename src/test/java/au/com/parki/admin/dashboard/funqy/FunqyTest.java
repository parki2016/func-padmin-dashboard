package au.com.parki.admin.dashboard.funqy;

import au.com.parki.admin.dashboard.funqy.dto.DashboardRequest;
import au.com.parki.admin.dashboard.funqy.dto.DashboardZoneResponse;
import io.quarkus.amazon.lambda.test.LambdaClient;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

@QuarkusTest
public class FunqyTest {
    @Test
    public void testSimpleLambdaSuccess() throws Exception {
        DashboardRequest in = new DashboardRequest();
        in.setToken("eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbmh1bnRlcmN3IiwiaXNzIjoiUGFya2lBZG1pbiIsImV4cCI6MTYzMDQ2ODUxMCwib3JnIjoxMDAwMDR9.9nwV1OeLfApHzsql9Tsi7Xz7j5-SkQZPze-PJf-Bu4s");
        List<DashboardZoneResponse> out = LambdaClient.invoke(List.class, in);
        Assertions.assertTrue(!out.isEmpty() && out.size() > 0);
    }
}
